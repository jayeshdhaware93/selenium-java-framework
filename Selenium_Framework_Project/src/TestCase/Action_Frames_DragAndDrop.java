package TestCase;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import Common_functions.Different_UI_functions_repositery;

public class Action_Frames_DragAndDrop extends Different_UI_functions_repositery {

	public static void main(String[] args) throws InterruptedException {

		WebDriver driver = instantiateDriver("Chrome");
		launchWebsite(driver, "https://www.globalsqa.com/demo-site/draganddrop/");

//      method 1: Direct method
		/*
		 * Actions action = new Actions(driver);
		 * 
		 * WebElement switch_element = get_element(driver,
		 * "//iframe[@data-src=\"../../demoSite/practice/droppable/photo-manager.html\"]",
		 * 5); driver.switchTo().frame(switch_element);
		 * 
		 * WebElement draggableElement = get_element(driver,
		 * "//li[@class=\"ui-widget-content ui-corner-tr ui-draggable ui-draggable-handle\"][1]"
		 * , 5); WebElement droppableElement = get_element(driver,
		 * "//div[@class=\"ui-widget-content ui-state-default ui-droppable\"]", 5);
		 * 
		 * action.dragAndDrop(draggableElement, droppableElement).build().perform();
		 * driver.switchTo().parentFrame();
		 */

//      method 2:using common functions

		switch_frame(driver, "//iframe[@data-src=\"../../demoSite/practice/droppable/photo-manager.html\"]", 5);
		dragAndDrop(driver, "//li[@class=\"ui-widget-content ui-corner-tr ui-draggable ui-draggable-handle\"][4]",
				"//div[@class=\"ui-widget-content ui-state-default ui-droppable\"]", 5);
		driver.switchTo().parentFrame();

		driver.quit();

	}

}
