package TestCase;

import java.io.File;
import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Common_functions.Utilities_Repositery;

public class UpperCase_Action extends Utilities_Repositery {

	WebDriver driver;
	File evidencefile;

	@BeforeTest
	public void presetup() throws InterruptedException {

		evidencefile = createLogDirectory("flipkartuppercase");
		driver = instantiateDriver("Chrome");
		launchWebsite(driver, "https://www.flipkart.com/");
	}

	@Test
	public void executor() {
		upper_case(driver, "//input[@class=\"Pke_EE\"]", "laptop", 5);
	}

	@AfterTest
	public void evidencecreation() throws IOException {
		takeScreenShot(driver, evidencefile, pagename(driver.getTitle()));
		driver.quit();
	}

}
