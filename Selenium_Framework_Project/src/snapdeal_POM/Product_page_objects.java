package snapdeal_POM;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import Common_functions.Utilities_Repositery;

public class Product_page_objects extends Utilities_Repositery {

	WebDriver driver;

	public Product_page_objects(WebDriver driver) {
		super();
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "(//ul[@id=\"bx-slider-left-image-panel\"]/li/*)[1]")
	WebElement product_image_zoom;

	public WebElement productImageZoom() {
		return product_image_zoom;
	}

	@FindBy(xpath = "//a[@class=\"bx-next sd-icon sd-icon-expand-arrow\"]")
	WebElement downword_arrow;

	public WebElement downWordArrow() {
		return downword_arrow;
	}

	@FindBy(xpath = "//h1[@itemprop=\"name\"]")
	WebElement product_detail_text;

	public WebElement productDetailText() {
		return product_detail_text;
	}

	@FindBy(xpath = "//div[@class=\"pdp-e-i-ratings\"]/div/span[4]")
	WebElement star_rating;

	public WebElement starRating() {
		return star_rating;
	}

	@FindBy(xpath = "//span[@class=\"pdp-final-price\"]/span")
	WebElement price_digit;

	public WebElement priceDigit() {
		return price_digit;
	}

	@FindBy(xpath = "//input[@placeholder=\"Enter pincode\"]")
	WebElement enter_pincode_box;

	public WebElement pincodeBox() {
		return enter_pincode_box;
	}

	@FindBy(xpath = "//div[@id=\"pincode-check-bttn\"]")
	WebElement check_button;

	public WebElement checkButton() {
		return check_button;
	}

	@FindBy(xpath = "(//div[@class=\"tabs clearfix\"]/ul/li)[1]")
	WebElement itemDetails_hylink;

	public WebElement itemDetailsHylink() {
		return itemDetails_hylink;
	}

	@FindBy(xpath = "//div[@class=\"tab-container\"]")
	WebElement item_detailhyperlink_text;

	public WebElement itemDetailHylinkText() {
		return item_detailhyperlink_text;
	}

	@FindBy(xpath = "//div[contains(@class,\"buyLink buyNow  \")]")
	WebElement buynow_button;

	public WebElement buyNowButton() {
		return buynow_button;
	}

}
