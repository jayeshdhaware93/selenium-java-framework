package snapdeal_POM;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import Common_functions.Utilities_Repositery;

public class Select_product_objects extends Utilities_Repositery {

	WebDriver driver;

	public Select_product_objects(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//div[@data-dpwlbl=\"Homepage Top Hook\"]")
	WebElement topPanel;

	public WebElement toppanel() {
		return topPanel;
	}

	@FindBy(xpath = "//input[@id=\"inputValEnter\"]")
	WebElement searchboxclick;

	public WebElement searchboxclick() {
		return searchboxclick;
	}

	@FindBy(xpath = "//button[@onclick=\"submitSearchForm('go_header');\"]")
	WebElement searchbutton;

	public WebElement searchbutton() {
		return searchbutton;
	}

	@FindBy(xpath = "(//span[@style=\"color: #212121; font-weight: normal\"])[1]")
	WebElement aftersearch_text;

	public WebElement aftersearchtext() {
		return aftersearch_text;
	}

	@FindBy(xpath = "(//a[@catid=\"46139343\"]/div)[1]")
	WebElement smartwearables_hylink;

	public WebElement smartwearables_hylink() {
		return smartwearables_hylink;
	}

	@FindBy(xpath = "(//input[@class=\"input-filter\"])[1]")
	WebElement minimumprice_box_click;

	public WebElement minimumpricebox() {
		return minimumprice_box_click;
	}

	@FindBy(xpath = "(//input[@class=\"input-filter\"])[2]")
	WebElement maximumprice_box_click;

	public WebElement maximumpricebox() {
		return maximumprice_box_click;
	}

	@FindBy(xpath = "//div[contains(@class,\"price-go-arrow\")]")
	WebElement go_box_click;

	public WebElement goBox() {
		return go_box_click;
	}

	@FindBy(xpath = "//label[@for=\"avgRating-4.0\"]")
	WebElement four_star_rating;

	public WebElement fourStarRating() {
		return four_star_rating;
	}

	@FindBy(xpath = "(//section[@data-dpwlbl=\"Product Grid\"]/div)[4]")
	WebElement product_image_hover;

	public WebElement productImageHover() {
		return product_image_hover;
	}

	@FindBy(xpath = "//i[@class=\"sd-icon sd-icon-expand-arrow sort-arrow\"]")
	WebElement sortby_dropdown;

	public WebElement sortByDropdown() {
		return sortby_dropdown;
	}

	@FindBy(xpath = "//ul[@class=\"sort-value\"]/li[2]")
	WebElement category2;

	public WebElement category2() {
		return category2;
	}

	@FindBy(xpath = "//ul[@class=\"sort-value\"]/li[1]")
	WebElement desired_category;

	public WebElement desiredCategory() {
		return desired_category;
	}

	@FindBy(xpath = "//div[@class=\"sorting-sec animBounce\"]/ul/li")
	WebElement sortby_options_list;

}
