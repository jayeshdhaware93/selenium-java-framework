package snapdeal_POM;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import Common_functions.Utilities_Repositery;

public class Select_product_objects2 extends Utilities_Repositery {

	WebDriver driver;
	Actions act;

	public Select_product_objects2(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//div[@data-dpwlbl=\"Homepage Top Hook\"]")
	WebElement topPanel;

	@FindBy(xpath = "//input[@id=\"inputValEnter\"]")
	WebElement searchboxclick;

	@FindBy(xpath = "//button[@onclick=\"submitSearchForm('go_header');\"]")
	WebElement searchbutton;

	@FindBy(xpath = "//div[@class=\"search-result-header\"]/div/div/div/span")
	WebElement aftersearch_text;

	@FindBy(xpath = "//div[text()=\"Smart Wearables\"]")
	WebElement smartwearables_hylink;

	@FindBy(xpath = "(//input[@class=\"input-filter\"])[1]")
	WebElement minimumprice_box_click;

	@FindBy(xpath = "(//input[@class=\"input-filter\"])[2]")
	WebElement maximumprice_box_click;

	@FindBy(xpath = "//div[contains(@class,\"price-go-arrow\")]")
	WebElement go_box_click;

	@FindBy(xpath = "(//section[@data-dpwlbl=\"Product Grid\"]/div)[4]")
	WebElement product_image_hover;

	@FindBy(xpath = "//i[@class=\"sd-icon sd-icon-expand-arrow sort-arrow\"]")
	WebElement sortby_dropdown;

	@FindBy(xpath = "//li[@data-sorttype=\"dhtl\"]")
	WebElement category2;

	@FindBy(xpath = "//li[@data-sorttype=\"rlvncy\"]")
	WebElement desired_category;

	@FindBy(xpath = "//div[@class=\"sorting-sec animBounce\"]/ul/li")
	WebElement sortby_options_list;

//	webpage specicfic methods

//	scroll back to top
	public void toppanel() {
		scroll_element(driver, topPanel, 10);
	}

//	click on search box and enter the keyword
	public void searchboxclick() {
		searchBox_enterText_element(driver, searchboxclick, "smart watch", 5);
	}

//	click on search button
	public void searchbutton() {
		clickElement(driver, searchbutton, 5);
	}

//	verify and print after search notification text
	public void aftersearchtext() {
		System.out.println(getElement_element(driver, aftersearch_text, 5).getText());

	}

//	click on smart wearables hyper link
	public void smartwearables_hylink() {
		clickElement(driver, smartwearables_hylink, 5);
	}

//	enter the price amount in minimum price range box
	public void minimumpricebox() {
		act = new Actions(driver);
		act.moveToElement(minimumprice_box_click).doubleClick().keyDown(Keys.DELETE).keyUp(Keys.DELETE).sendKeys("500")
				.build().perform();
	}

//	enter the price amount in maximum price range box
	public void maximumpricebox() {
		act = new Actions(driver);
		act.moveToElement(maximumprice_box_click).doubleClick().keyDown(Keys.DELETE).keyUp(Keys.DELETE).sendKeys("4000")
				.build().perform();

	}

//	click on go button box
	public void goBox() {
		clickElement(driver, go_box_click, 5);
	}

//	click on sort by button
	public void sortByDropdown() {
		clickElement(driver, sortby_dropdown, 5);
	}

//	select second category
	public void category2() {
		clickElement(driver, category2, 5);
	}

//	click on desired category
	public void desiredCategory() {
		clickElement(driver, desired_category, 5);
	}

//	hover to desired product from product list
	public void productImageHover() {
		hoverTo_element(driver, product_image_hover, 5);
	}

//	take screenshot of hovered product image
	public void screenshot(File logdir) throws IOException {
		takeScreenShot(driver, logdir, "desired_product");
	}

//	select and click on desired product
	public void clickOnProduct() {
		clickElement(driver, product_image_hover, 5);
	}

//	switch to next new tab opened
	public void switchToProductTab() {
		Set<String> window = driver.getWindowHandles();
		Iterator<String> newtabs = window.iterator();
		while (newtabs.hasNext()) {
			driver.switchTo().window(newtabs.next());
		}

	}

}
