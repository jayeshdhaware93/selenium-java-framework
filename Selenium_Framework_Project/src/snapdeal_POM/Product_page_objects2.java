package snapdeal_POM;

import java.io.File;
import java.io.IOException;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import Common_functions.Utilities_Repositery;

public class Product_page_objects2 extends Utilities_Repositery {

	WebDriver driver;
	Actions act;
	JavascriptExecutor jse;

	public Product_page_objects2(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//div[@class=\"col-xs-21 reset-padding height-inherit\"]")
	WebElement product_image_zoom;

	@FindBy(xpath = "//a[@class=\"bx-next sd-icon sd-icon-expand-arrow\"]")
	WebElement downword_arrow;

	@FindBy(xpath = "//h1[@itemprop=\"name\"]")
	WebElement product_detail_text;

	@FindBy(xpath = "//span[@class=\"avrg-rating\"]")
	WebElement star_rating;

	@FindBy(xpath = "//span[@class=\"pdp-final-price\"]/span")
	WebElement price_digit;

	@FindBy(xpath = "//input[@placeholder=\"Enter pincode\"]")
	WebElement enter_pincode_box;

	@FindBy(xpath = "//div[@id=\"pincode-check-bttn\"]")
	WebElement check_button;

	@FindBy(xpath = "(//div[@class=\"tabs clearfix\"]/ul/li)[1]")
	WebElement itemDetails_hylink;

	@FindBy(xpath = "//div[@class=\"tab-container\"]")
	WebElement item_detailhyperlink_text;

	@FindBy(xpath = "//div[contains(@class,\"buyLink buyNow  \")]")
	WebElement buynow_button;

//	 webpage specific pom methods

	public void screenshot(File logdir) throws IOException {
		takeScreenShot(driver, logdir, "product_selected");
	}

//	hover on product image
	public void productImageZoom() {
		hoverTo_element(driver, product_image_zoom, 5);
	}

//	click on downword arrow
	public void downWordArrow() {
		object_handling_element(driver, downword_arrow, 5);
	}

//	get product detail text
	public void productDetailText() {
		System.out.println(getText_element(driver, product_detail_text, 5));
	}

//	double click on star ratings
	public void starRating() {
		act = new Actions(driver);
		act.moveToElement(star_rating).doubleClick().build().perform();
	}

//	fetch star rating of product in numbers
	public void ratingText() {
		System.out.println(getText_element(driver, star_rating, 5));
	}

//	fetch product price
	public void priceDigit() {
		System.out.println(getText_element(driver, price_digit, 5));
	}

//	scroll to item detail hyperlink
	public void itemDetailsHylink() {
		jse = (JavascriptExecutor) driver;
		jse.executeScript("arguments[0].scrollIntoView(true);", itemDetails_hylink);
	}

//	print item details
	public void itemDetailHylinkText() {
		System.out.println(getText_element(driver, item_detailhyperlink_text, 5));
	}

//	scroll to top
	public void scrollToTop() {
		jse = (JavascriptExecutor) driver;
		jse.executeScript("arguments[0].scrollIntoView(true);", product_detail_text);
	}

//	enter pincode in pincode check box
	public void pincodeBox() {
		act = new Actions(driver);
		act.moveToElement(enter_pincode_box).click().sendKeys("413508").build().perform();
	}

//	click on check box
	public void checkButton() {
		clickElement(driver, check_button, 5);
	}

//	click on buy now button
	public void buyNowButton() {
		clickElement(driver, buynow_button, 5);
	}
}
