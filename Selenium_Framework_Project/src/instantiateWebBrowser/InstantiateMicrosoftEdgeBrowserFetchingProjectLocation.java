package instantiateWebBrowser;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;

public class InstantiateMicrosoftEdgeBrowserFetchingProjectLocation {

	public static void main(String[] args) {

//		fetch currant project directory using system.getproperty

		String project_dir = System.getProperty("user.dir");

//		give address of edgedriver using system.setproperty to our program by fetching project directory using system.getproperty

		System.setProperty("webdriver.msedge.driver", project_dir + "\\drivers\\msedgedriver.exe");

//		create refernce of edgedriver class

		WebDriver driver = new EdgeDriver();

//		launch website of any choise

		driver.get("https://www.amazon.in/");
		driver.manage().window().maximize();

//		close the edgedriver

		driver.quit();
	}

}
