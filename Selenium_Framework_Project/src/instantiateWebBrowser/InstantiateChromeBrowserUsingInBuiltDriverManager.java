 package instantiateWebBrowser;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class InstantiateChromeBrowserUsingInBuiltDriverManager {

	public static void main(String[] args) {

		// Web driver manager automatically detects the version of Browser and downloads
		// corresponding driver at location (C:\Users\ADMIN or user
		// folder\.cache\selenium) and sets the system
		// property

//		create reference of chromedriver class

		WebDriver driver = new ChromeDriver();

//		launch website of any choise

		driver.get("https://www.youtube.com/");
		driver.manage().window().maximize();

//		close the chromedriver
		driver.quit();

	}
}