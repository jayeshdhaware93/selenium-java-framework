package instantiateWebBrowser;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class InstantiateChromeBrowserWithGivenLocation {

	public static void main(String[] args) {

//		give address of chromedriver from local address to our program by using system.setproperty

		System.setProperty("webdriver.chrome.driver",
				"E:\\MSSquareClass\\WebDrivers\\chrome\\chromedriver-win64\\chromedriver.exe");

//		create reference of chromedriver class

		WebDriver driver = new ChromeDriver();

//		launch any website

		driver.get("https://www.goibibo.com/");
		driver.manage().window().fullscreen();

//		close the chromedriver

		driver.quit();
	}

}
