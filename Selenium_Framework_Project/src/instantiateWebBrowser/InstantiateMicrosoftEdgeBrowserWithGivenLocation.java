package instantiateWebBrowser;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;

public class InstantiateMicrosoftEdgeBrowserWithGivenLocation {

	public static void main(String[] args) {

//		give address of  edgewebdriver from local storage to our program by using system.setproperty

		System.setProperty("webdriver.msedge.driver", "E:\\MSSquareClass\\WebDrivers\\MicrosoftEdge\\msedgedriver.exe");

//		create reference of edgedriver class

		WebDriver driver = new EdgeDriver();

//		launch website of any choise

		driver.get("https://www.goibibo.com/");
		driver.manage().window().fullscreen();

//		close the edgedriver

		driver.quit();

	}

}
