package instantiateWebBrowser;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class InstantiateChromeBrowserFetchingProjectLocation {

	public static void main(String[] args) {

//		fetch project directory

		String project_dir = System.getProperty("user.dir");

//		give address of chrome  webdriver using system.setproperty by fetching the project location using system.setproperty 

		System.setProperty("webdriver.chrome.driver", project_dir + "\\drivers\\chromedriver.exe");

//		create reference of chromedriver class

		WebDriver driver = new ChromeDriver();

//		launch website of any choise

		driver.get("https://www.amazon.in/");
		driver.manage().window().maximize();

//        close the chromedriver

		driver.quit();
	}

}
