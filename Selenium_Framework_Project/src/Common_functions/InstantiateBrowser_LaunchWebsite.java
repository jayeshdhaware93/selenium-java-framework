package Common_functions;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

public class InstantiateBrowser_LaunchWebsite {

	public static WebDriver instantiateDriver(String browser) {
		WebDriver driver;

		switch (browser) {

		case "Chrome":
			driver = new ChromeDriver();
			break;

		case "Edge":
			driver = new EdgeDriver();
			break;

		case "FireFox":
			driver = new FirefoxDriver();
			break;

		case "ChromeHeadless":
			ChromeOptions choption = new ChromeOptions();
			choption.addArguments("--headless");
			driver = new ChromeDriver(choption);
			break;

		case "EdgeHeadless":
			EdgeOptions edgeoption = new EdgeOptions();
			edgeoption.addArguments("--headless");
			driver = new EdgeDriver(edgeoption);

		case "FireFoxHeadless":
			FirefoxOptions FFoption = new FirefoxOptions();
			FFoption.addArguments("--headless");
			driver = new FirefoxDriver(FFoption);

		default:
			driver = new ChromeDriver();
			break;
		}

		return driver;
	}

	public static void launchWebsite(WebDriver driver, String URL) {
		driver.get(URL);

	}

}
