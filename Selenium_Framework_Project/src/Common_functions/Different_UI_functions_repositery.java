package Common_functions;

import java.time.Duration;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Different_UI_functions_repositery extends InstantiateBrowser_LaunchWebsite {

	public static void click(WebDriver driver, String clickButtonXpath, int timeout) {
		WebDriverWait exp_wait = new WebDriverWait(driver, Duration.ofSeconds(timeout));
		exp_wait.until(ExpectedConditions.elementToBeClickable(By.xpath(clickButtonXpath))).click();
	}

	public static void clickElement(WebDriver driver, WebElement clickElement, int timeout) {
		WebDriverWait exp_wait = new WebDriverWait(driver, Duration.ofSeconds(timeout));
		exp_wait.until(ExpectedConditions.elementToBeClickable(clickElement)).click();
	}

	public static void searchBox_enterText(WebDriver driver, String searchBoxXpath, String userInputSearchText,
			int timeout) {
		WebDriverWait exp_wait = new WebDriverWait(driver, Duration.ofSeconds(timeout));
		exp_wait.until(ExpectedConditions.elementToBeClickable(By.xpath(searchBoxXpath))).sendKeys(userInputSearchText);
	}

	public static void searchBox_enterText_element(WebDriver driver, WebElement searchboxelement,
			String userInputSearchText, int timeout) {
		WebDriverWait exp_wait = new WebDriverWait(driver, Duration.ofSeconds(timeout));
		exp_wait.until(ExpectedConditions.elementToBeClickable(searchboxelement)).sendKeys(userInputSearchText);
	}

	public static String searchResultText(WebDriver driver, String textXpath, int timeout) {
		WebDriverWait exp_wait = new WebDriverWait(driver, Duration.ofSeconds(timeout));
		String searchtext = exp_wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(textXpath)))
				.getText();
		return searchtext;
	}

	public static String searchResultText_element(WebDriver driver, WebElement textElement, int timeout) {
		WebDriverWait exp_wait = new WebDriverWait(driver, Duration.ofSeconds(timeout));
		String searchtext = exp_wait.until(ExpectedConditions.visibilityOf(textElement)).getText();
		return searchtext;
	}

	public static String getText_element(WebDriver driver, WebElement textElement, int timeout) {
		WebDriverWait exp_wait = new WebDriverWait(driver, Duration.ofSeconds(timeout));
		String text = exp_wait.until(ExpectedConditions.visibilityOf(textElement)).getText();
		return text;
	}

	public static WebElement get_element(WebDriver driver, String xpath, int timeout) {
		WebDriverWait exp_wait = new WebDriverWait(driver, Duration.ofSeconds(timeout));
		WebElement element = exp_wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
		return element;
	}

	public static WebElement getElement_element(WebDriver driver, WebElement elementText, int timeout) {
		WebDriverWait exp_wait = new WebDriverWait(driver, Duration.ofSeconds(timeout));
		WebElement element = exp_wait.until(ExpectedConditions.visibilityOf(elementText));
		return element;
	}

	public static List<WebElement> get_elements(WebDriver driver, String xpath, int timeout) {
		WebDriverWait exp_wait = new WebDriverWait(driver, Duration.ofSeconds(timeout));
		List<WebElement> elements = exp_wait
				.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(xpath)));
		return elements;
	}

	public static List<WebElement> getelements_element(WebDriver driver, WebElement elementtexts, int timeout) {
		WebDriverWait exp_wait = new WebDriverWait(driver, Duration.ofSeconds(timeout));
		List<WebElement> elements = exp_wait.until(ExpectedConditions.visibilityOfAllElements(elementtexts));
		return elements;
	}

	public static void dropDown_options(WebDriver driver, String searchBoxpath, String inputToSearch,
			String optionspath, String optionclickpath, String optionToSelect, int timeout) {

		searchBox_enterText(driver, searchBoxpath, inputToSearch, timeout);
		List<WebElement> DD_options = get_elements(driver, optionspath, timeout);
		int count = DD_options.size();
		System.out.println("no of suggetions:" + count);

		for (int i = 0; i < count; i++) {

			String DD_options_Text = DD_options.get(i).getText();
			System.out.println(DD_options_Text);

			if (DD_options_Text.equals(optionToSelect)) {
				DD_options.get(i).click();
				break;
			}
		}
	}

	public static void dropDown_options_element(WebDriver driver, WebElement searchboxelement, String inputToSearch,
			WebElement optionselement, WebElement optionclielement, String optionToSelect, int timeout) {

		searchBox_enterText_element(driver, searchboxelement, inputToSearch, timeout);
		List<WebElement> DD_options = getelements_element(driver, optionselement, timeout);
		int count = DD_options.size();
		System.out.println("no of suggetions:" + count);

		for (int i = 0; i < count; i++) {

			String DD_options_Text = DD_options.get(i).getText();
			System.out.println(DD_options_Text);

			if (DD_options_Text.equals(optionToSelect)) {
				DD_options.get(i).click();
				break;
			}
		}
	}

	public static void calender_Date_options(WebDriver driver, String calenderpath, String dateOptionsPath,
			String clickDatePath, String dateToSelect, int timeout) {

		List<WebElement> date_options = get_elements(driver, dateOptionsPath, timeout);
		int count = date_options.size();

		for (int i = 0; i < count; i++) {
			String date_options_text = date_options.get(i).getText();
			System.out.println(date_options_text);

			if (date_options_text.equals(dateToSelect)) {
				date_options.get(i).click();
				break;
			}
		}
	}

	public static void calender_DateOptions_element(WebDriver driver, WebElement calenderelement,
			WebElement dateOptionselement, WebElement clickDatelement, String dateToSelect, int timeout) {

		List<WebElement> date_options = getelements_element(driver, dateOptionselement, timeout);
		int count = date_options.size();

		for (int i = 0; i < count; i++) {
			String date_options_text = date_options.get(i).getText();
			System.out.println(date_options_text);

			if (date_options_text.equals(dateToSelect)) {
				date_options.get(i).click();
				break;
			}
		}
	}

	public static void upper_case(WebDriver driver, String searchboxpath, String textToSearch, int timeout) {
		Actions action = new Actions(driver);
		WebElement element = get_element(driver, searchboxpath, timeout);
		action.moveToElement(element).click().keyDown(Keys.SHIFT).sendKeys(textToSearch).keyUp(Keys.SHIFT)
				.keyDown(Keys.ENTER).keyUp(Keys.ENTER).build().perform();

	}

	public static void upper_case_element(WebDriver driver, WebElement searchboxelement, String textToSearch,
			int timeout) {
		Actions action = new Actions(driver);
		WebElement element = getElement_element(driver, searchboxelement, timeout);
		action.moveToElement(element).click().keyDown(Keys.SHIFT).sendKeys(textToSearch).keyUp(Keys.SHIFT)
				.keyDown(Keys.ENTER).keyUp(Keys.ENTER).build().perform();

	}

	public static void right_click(WebDriver driver, String xpath, int timeout) {
		Actions action = new Actions(driver);
		WebElement element = get_element(driver, xpath, timeout);
		action.moveToElement(element).contextClick().build().perform();

	}

	public static void right_click_element(WebDriver driver, WebElement element, int timeout) {
		Actions action = new Actions(driver);
		WebElement rightclick_element = getElement_element(driver, element, timeout);
		action.moveToElement(rightclick_element).contextClick().build().perform();

	}

	public static void double_click(WebDriver driver, String xpath, int timeout) {
		Actions action = new Actions(driver);
		WebElement element = get_element(driver, xpath, timeout);
		action.moveToElement(element).doubleClick().build().perform();
	}

	public static void double_click_element(WebDriver driver, WebElement element, int timeout) {
		Actions action = new Actions(driver);
		WebElement doubleclick_element = getElement_element(driver, element, timeout);
		action.moveToElement(doubleclick_element).doubleClick().build().perform();
	}

	public static void switch_frame(WebDriver driver, String framexpath, int timeout) {
		WebElement frame_element = get_element(driver, framexpath, timeout);
		driver.switchTo().frame(frame_element);
	}

	public static void switch_frame_element(WebDriver driver, WebElement frameelement, int timeout) {
		WebElement frame_element = getElement_element(driver, frameelement, timeout);
		driver.switchTo().frame(frame_element);
	}

	public static void dragAndDrop(WebDriver driver, String draggablepath, String droppablepath, int timeout) {
		Actions action = new Actions(driver);
		WebElement draggable_element = get_element(driver, draggablepath, timeout);
		WebElement droppable_element = get_element(driver, droppablepath, timeout);
		action.dragAndDrop(draggable_element, droppable_element).build().perform();
	}

	public static void dragAndDrop_element(WebDriver driver, WebElement draggableelement, WebElement droppableElement,
			int timeout) {
		Actions action = new Actions(driver);
		WebElement draggable_element = getElement_element(driver, draggableelement, timeout);
		WebElement droppable_element = getElement_element(driver, droppableElement, timeout);
		action.dragAndDrop(draggable_element, droppable_element).build().perform();
	}

	public static void scroll(WebDriver driver, String xpath, int timeout) {

		JavascriptExecutor jse = (JavascriptExecutor) driver;
		WebElement element = get_element(driver, xpath, timeout);
		jse.executeScript("arguments[0].scrollIntoView(true)", element);
	}

	public static void scroll_element(WebDriver driver, WebElement element, int timeout) {

		JavascriptExecutor jse = (JavascriptExecutor) driver;
		WebElement scroll_element = getElement_element(driver, element, timeout);
		jse.executeScript("arguments[0].scrollIntoView(true)", scroll_element);
	}

	public static void hoverTo_element(WebDriver driver, WebElement element, int timeout) {
		Actions action = new Actions(driver);
		action.moveToElement(element).build().perform();
	}

	public static void multi_tab_open(WebDriver driver, String xpath, int timeout) {

		Actions action = new Actions(driver);
		List<WebElement> element = get_elements(driver, xpath, timeout);
		int count = element.size();
		System.out.println("no of hyperlinks present:" + count);
		for (int i = 0; i < count; i++) {
			action.moveToElement(element.get(i)).keyDown(Keys.CONTROL).click().keyUp(Keys.CONTROL).build().perform();

		}
	}

	public static void multi_tab_open_element(WebDriver driver, WebElement element, int timeout) {

		Actions action = new Actions(driver);
		List<WebElement> multitab_element = getelements_element(driver, element, timeout);
		int count = multitab_element.size();
		System.out.println("no of hyperlinks present:" + count);
		for (int i = 0; i < count; i++) {
			action.moveToElement(multitab_element.get(i)).keyDown(Keys.CONTROL).click().keyUp(Keys.CONTROL).build()
					.perform();
		}
	}

	public static void windowSwitch_element(WebDriver driver, WebElement element, int timeout) {

		Set<String> window = driver.getWindowHandles();
		Iterator<String> newtabs = window.iterator();
		while (newtabs.hasNext()) {
			driver.switchTo().window(newtabs.next());
		}
	}

	public static void popup_handling(WebDriver driver, String xpath, int timeout) {
		try {
			click(driver, xpath, timeout);
		} catch (TimeoutException te) {
			System.out.println("popup not found hence progressing ahead");
			te.printStackTrace();
		} finally {
			System.out.println("popup handled succesfully");

		}
	}

	public static void popup_handling_element(WebDriver driver, WebElement element, int timeout) {
		try {
			clickElement(driver, element, timeout);
		} catch (TimeoutException te) {
			System.out.println("exception not found hence continuing with testcase");
		} finally {
			System.out.println("popup handled successfully");
		}
	}

	public static void object_handling_element(WebDriver driver, WebElement element, int timeout) {
		try {
			clickElement(driver, element, timeout);
		} catch (Exception e) {
			System.out.println("desired object not visible,hence continuing with testcase");

		} finally {
			System.out.println("object visibility handled successfully");
		}
	}

}
